package es.vidal.actividad7;

import java.time.LocalDate;

public class Persona {

    private String nombreApellidos;

    private String dni;

    private String email;

    private LocalDate anyoNacimiento;

    public Persona(String nombreApellidos, String dni, String email, LocalDate anyoNacimiento) {
        this.nombreApellidos = nombreApellidos;
        this.dni = dni;
        this.email = email;
        this.anyoNacimiento = anyoNacimiento;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "Nombre y apellidos: " + nombreApellidos  +
                ", Dni: " + dni +
                ", Email: " + email +
                ", Data de Naixement: " + anyoNacimiento +
                '}';
    }
}
