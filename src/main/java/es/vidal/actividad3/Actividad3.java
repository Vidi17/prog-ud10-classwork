package es.vidal.actividad3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Actividad3 {
    public static void main(String[] args) {
        File directory = new File("resources/actividad3");

        File archive = new File(directory, "fichero3.txt");

        try {
            archive.createNewFile();
            FileWriter fileWriter = new FileWriter(archive);
            fileWriter.write("Jordi Vidal Cerdà");
            fileWriter.close();
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
