package es.vidal.actividad4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Actividad4 {
    public static void main(String[] args) {
        File file = new File("resources/actividad4/fichero4.txt");

        HashMap<Integer, String> lista = leerArchivo(file);

        mostrarTodosProductos(lista);

        int opcion = obtenerOpcionUsuario();

        mostrarProducto(lista, opcion);

    }

    public static void mostrarTodosProductos(HashMap<Integer, String> lista){
        System.out.println(lista);
    }

    public static void mostrarProducto(HashMap<Integer, String> lista, int opcion){
        System.out.println(lista.getOrDefault(opcion, "No disponemos de ese producto"));
    }

    public static int obtenerOpcionUsuario(){
        Scanner teclado = new Scanner(System.in);
        System.out.print("introduce el número del producto: ");
        return teclado.nextInt();
    }

    public static HashMap<Integer, String> leerArchivo(File file){
        HashMap<Integer, String> lista = new HashMap<>();
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String linea;
            do {
                linea = bufferedReader.readLine();
                if (linea != null){
                    anyadirLinea(lista, linea);
                }
            }while (linea != null);

        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }finally {
            try {
                bufferedReader.close();
            }catch (IOException e){
                e.getStackTrace();
            }
        }
        return lista;
    }

    public static void anyadirLinea(HashMap<Integer, String> lista, String linea){
        String[] lineaPartida;
        lineaPartida = (linea.split(","));
        int id = Integer.parseInt(lineaPartida[0]);
        String valor = lineaPartida[1].trim();
        lista.put(id, valor);
    }
}
