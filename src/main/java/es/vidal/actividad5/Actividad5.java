package es.vidal.actividad5;

import java.io.*;
import java.util.ArrayList;
import java.util.Locale;

public class Actividad5 {
    public static void main(String[] args) {
        File file1 = new File("resources/actividad5/fichero5.txt");
        File file2 = new File("resources/actividad5/fichero6.txt");
        ArrayList<String> lineasArchivo;

        lineasArchivo = leerArchivo(file1);
        anyadirLineas(lineasArchivo, file2);

        try {
            file2.createNewFile();
        }catch (IOException ex) {
            ex.getStackTrace();
        }
    }

    public static void anyadirLineas(ArrayList<String> lineasArchivo, File file){
        BufferedWriter bufferedWriter = null;

        try {
            FileWriter fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            for (String linea: lineasArchivo) {
                bufferedWriter.write(linea);
                bufferedWriter.newLine();
            }
        }catch (IOException e){
            e.getStackTrace();
        }finally {
            try {
                bufferedWriter.close();
            }catch (IOException e){
                e.getStackTrace();
            }
        }
    }

    public static ArrayList<String> leerArchivo (File file){
        ArrayList<String> lineasArchivo = new ArrayList<>();
        BufferedReader bufferedReader = null;

        try {
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String linea;
            do {
                linea = bufferedReader.readLine();
                if (linea != null){
                    lineasArchivo.add(linea.toUpperCase(Locale.ROOT));
                }
            }while (linea != null);
        }catch (IOException ex){
            ex.getStackTrace();
        }finally {
            try {
                bufferedReader.close();
            }catch (IOException e){
                e.getStackTrace();
            }
        }
        return lineasArchivo;
    }
}
