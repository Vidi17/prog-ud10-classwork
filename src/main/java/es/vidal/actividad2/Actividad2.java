package es.vidal.actividad2;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Actividad2 {
    public static void main(String[] args) {

        File directory = new File("resources/actividad2");

        File archive = new File(directory, "fichero2.txt");

        StringBuilder fileContent = new StringBuilder();

        try {
            archive.createNewFile();
            FileReader fileReader = new FileReader("resources/actividad2/fichero2.txt");
            int character;
            do {
                character = fileReader.read();
                if (character >= 0) {
                    fileContent.append((char) character);
                } else {
                    fileReader.close();
                }
            } while (character > 0);
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
        System.out.println(fileContent);
    }
}
