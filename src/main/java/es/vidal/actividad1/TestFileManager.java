package es.vidal.actividad1;

import java.io.IOException;

public class TestFileManager {
    public static void main(String[] args) {
        try {
            FileManager.crearFichero("resources/actividad1", "Data.txt");
            FileManager.verDirectorio("resources/actividad1");
            FileManager.verInformacion("resources/actividad1", "Data.txt");
        }catch (IOException exception){
            System.out.println(exception.getMessage());
        }
    }
}
