package es.vidal.actividad1;

import java.io.File;
import java.io.IOException;


public class FileManager {

    public static void crearFichero(String directorio, String archivo) throws IOException {
        File directory = new File(directorio);
        if (!directory.exists()){
            throw new IOException("El directorio no existe");
        }
        File file = new File(directory, archivo);
        file.createNewFile();
    }

    public static void verDirectorio(String directorio){
        File directory = new File(directorio);
        if (!directory.exists() ){
            System.out.println("El directorio no existe");
        }else if (!directory.isDirectory()){
            System.out.println("No es un directorio");
        }else {
            File[] filesInDirectory = directory.listFiles();
            assert filesInDirectory != null;
            for (File fileToShow : filesInDirectory) {
                System.out.println("Archivos del directorio " + directorio + "\n" + fileToShow.getName());
            }
            System.out.println();
        }
    }

    public static void verInformacion(String directorio, String archivo){
        File file = new File(directorio, archivo);
        if (!file.exists()){
            System.out.println("El archivo no existe");
        }else if (!file.isFile()){
            System.out.println("No es un archivo");
        }else {
            System.out.printf("""
                            Nombre del archivo: %s
                            Ruta absoluta del archivo: %s
                            Ruta relativa del archivo: %s
                            Se puede escribir: %s
                            Se puede leer: %s
                            Es un archivo: %s
                            Es un directorio: %s""", file.getName(), file.getAbsolutePath()
                    , file.getPath(), file.canWrite(), file.canRead(), file.isFile()
                    , file.isDirectory());
        }

    }
}
