package es.vidal.actividad6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Actividad6 {
    public static void main(String[] args) {
        File file = new File("resources/actividad6/alumnos.txt");
        ArrayList<String> alumnos = leerArchivo(file);

        String[] alumno1 = separarAlumno(alumnos.get(0));
        String[] alumno2 = separarAlumno(alumnos.get(1));
        String[] alumno3 = separarAlumno(alumnos.get(2));

        ArrayList<Integer> fecha1 = separarFecha(alumno1[3]);
        ArrayList<Integer> fecha2 = separarFecha(alumno2[3]);
        ArrayList<Integer> fecha3 = separarFecha(alumno3[3]);

        Persona persona1 = new Persona(alumno1[0], alumno1[1], alumno1[2], LocalDate.of(fecha1.get(0), fecha1.get(1)
                , fecha1.get(2)));

        Persona persona2 = new Persona(alumno2[0], alumno2[1], alumno2[2], LocalDate.of(fecha2.get(0), fecha2.get(1)
                , fecha2.get(2)));

        Persona persona3 = new Persona(alumno3[0], alumno3[1], alumno3[2], LocalDate.of(fecha3.get(0), fecha3.get(1)
                , fecha3.get(2)));

        System.out.println(persona1);
        System.out.println(persona2);
        System.out.println(persona3);
    }

    public static  ArrayList<Integer> separarFecha(String fecha){
        String[] fechaSeparada = fecha.split("-");

        ArrayList<Integer> fechaEnteros = new ArrayList<>();

        fechaEnteros.add(Integer.parseInt(fechaSeparada[0]));
        fechaEnteros.add(Integer.parseInt(fechaSeparada[1]));
        fechaEnteros.add(Integer.parseInt(fechaSeparada[2]));

        return fechaEnteros;
    }

    public static String[] separarAlumno(String alumno){
        return alumno.split(";");
    }

    public static ArrayList<String> leerArchivo(File file){
        ArrayList<String> lineasArchivo = new ArrayList<>();

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            String linea;
            do {
                linea = bufferedReader.readLine();
                if (linea != null){
                    lineasArchivo.add(linea);
                }
            }while (linea != null);
        }catch (IOException ex){
            ex.getStackTrace();
        }
        return lineasArchivo;
    }
}
