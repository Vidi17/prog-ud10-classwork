package es.vidal.actividad8;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class FileUserDAO implements UserDaoInterface{

    private final File FILE;

    public FileUserDAO(){

        FILE = new File("resources/actividad8/fichero.txt");

    }

    @Override
    public ArrayList<UserDTO> findAll() throws IOException{
        ArrayList<UserDTO> usersList = new ArrayList<>();
        String line;
        try (FileReader fileReader = new FileReader(FILE);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    UserDTO userToAdd = createUserDTO(line);
                    usersList.add(userToAdd);
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return usersList;
    }

    public UserDTO createUserDTO(String line){
        String[] userDTO = line.split(";");
        String username = userDTO[0].trim();
        String password = userDTO[1].trim();
        String email =  userDTO[2].trim();
        LocalDate birthday = LocalDate.parse(userDTO[3].trim());
        return new UserDTO( username, password, email, birthday);
    }

    public String userToString(UserDTO userDTO){
        return userDTO.getUsername() +
                ";" +
                userDTO.getPassword() +
                ";" +
                userDTO.getEmail() +
                ";" +
                userDTO.getBirthday();
    }

    @Override
    public UserDTO findByEmail(String email) throws IOException {
        String line;
        try (FileReader fileReader = new FileReader(FILE);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    UserDTO userToAdd = createUserDTO(line);
                    if (userToAdd.getEmail().equals(email)){
                        return userToAdd;
                    }
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return null;
    }

    @Override
    public UserDTO getByMail(String email) throws UserNotFoundException , IOException{
        ArrayList<UserDTO> userDTOS = findAll();
        for (UserDTO userDTO: userDTOS) {
            if (userDTO.getEmail().equals(email)){
                return userDTO;
            }
        }
        throw new UserNotFoundException("El usuario no existe");
    }

    @Override
    public UserDTO findByUsername(String username) throws UserNotFoundException , IOException{
        ArrayList<UserDTO> userDTOS = findAll();
        for (UserDTO userDTO: userDTOS) {
            if (userDTO.getUsername().equals(username)){
                return userDTO;
            }
        }
        throw new UserNotFoundException("El usuario no existe");
    }

    @Override
    public ArrayList<UserDTO> findAdults(){
        ArrayList<UserDTO> userListAdults = new ArrayList<>();

        LocalDate localDateTimeNow = LocalDate.now();
        LocalDate localDateTimeCompare = localDateTimeNow.minusYears(18);

        try (FileReader fileReader = new FileReader("resources/A8/usuarios.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String userRegister;
            do {
                userRegister = bufferedReader.readLine();
                if (userRegister != null){
                    UserDTO userDTO = createUserDTO(userRegister);
                    if (userDTO.getBirthday() == null) {
                        return null;
                    }
                    if (localDateTimeCompare.isAfter(userDTO.getBirthday())) {
                        userListAdults.add(userDTO);
                    }
                }
            } while (userRegister != null);
        } catch (FileNotFoundException e) {
            System.out.println("El fichero que quieres leer no existe");
        } catch (IOException e) {
            System.out.println("Se ha producido un error en el acceso al fichero");
        }
        return userListAdults;
    }

    public void saveUser (UserDTO userDTO){
        try {
            ArrayList<UserDTO> usersList = findAll();
            if (usersList.contains(userDTO)){
                update(userDTO);
            }else {
                insert(userDTO);
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
    }

    private boolean insert(UserDTO userDTO) {
        try (FileWriter fileWriter = new FileWriter(FILE, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            String userToAdd = userToString(userDTO);
            bufferedWriter.append(userToAdd);
            bufferedWriter.newLine();
        }catch (IOException ex){
            ex.getStackTrace();
            return false;
        }
        return true;
    }

    public String[] userAsList(String user){
        return user.split(";");

    }

    @Override
    public boolean delete(String email) {
        ArrayList<String> usersList = new ArrayList<>();
        String userString;
        boolean deleted = false;

        try (FileReader fileReader = new FileReader(FILE);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                userString = bufferedReader.readLine();
                if (userString != null){
                    String[] user = userAsList(userString);

                    if (!user[2].trim().equals(email)){
                        usersList.add(userString);
                    }else {
                        deleted = true;
                    }
                }
            }while (userString != null);
        }catch (IOException ex){
            ex.getStackTrace();
        }

        try (FileWriter fileWriter = new FileWriter(FILE);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
             String user = usersList.get(0);
             bufferedWriter.write(user);
             bufferedWriter.newLine();

            for (int i = 1; i < usersList.size(); i++) {
                bufferedWriter.append(usersList.get(i));
                bufferedWriter.newLine();
            }
        }catch (IOException ex){
            ex.getStackTrace();
            return false;
        }
        return deleted;
    }

    private void update(UserDTO userDTO) {
        if (!delete(userDTO.getEmail())){
            insert(userDTO);
        }
    }
}
