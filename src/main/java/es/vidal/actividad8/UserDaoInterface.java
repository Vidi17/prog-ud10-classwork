package es.vidal.actividad8;

import java.io.IOException;
import java.util.ArrayList;

public interface UserDaoInterface {

    /**
     * Devuelve todos los usuarios presentes en el fichero
     *
     * @return ArrayList<UserDTO>
     */
    ArrayList<UserDTO> findAll() throws IOException;

    /**
     * Devuelve un usuario cuyo email correspode con @email
     *
     * @return UserDTO
     */
    UserDTO findByEmail(String emailToSearch) throws IOException;

    /**
     * Devuelve un usuario cuyo email correspode con @email si no lo encuentra
     * lanza una excepción @UserNotFoundException
     *
     * @return UserDTO
     */
    UserDTO getByMail(String emailToSearch) throws UserNotFoundException, IOException;

    /**
     * Devuelve un usuario cuyo username correspode con @username
     *
     * @return UserDTO
     */
    UserDTO findByUsername(String username) throws IOException, UserNotFoundException;

    /**
     * Devuelve un listado de todos los usuarios mayores de 18 años
     *
     * @return ArrayList<UserDTO>
     */
    ArrayList<UserDTO> findAdults();

    /**
     * Inserta un nuevo usuario en la bd
     *
     * @return boolean indicando si la operacion se ha realizado correctamente
     */
    boolean delete(String email);


}
