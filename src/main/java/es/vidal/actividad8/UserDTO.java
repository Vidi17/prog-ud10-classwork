package es.vidal.actividad8;

import java.time.LocalDate;
import java.util.Objects;

public class UserDTO {

    private String username;

    private String password;

    private String email;

    private LocalDate birthday;

    public UserDTO(String username, String password, String email, LocalDate birthday) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "username: " + username +
                ", password: " + password +
                ", email: " + email +
                ", birthday: " + birthday +
                "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return email.equals(userDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
