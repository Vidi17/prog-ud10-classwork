package es.vidal.actividad8;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class TestFileUserDao {
    public static void main(String[] args) {

        FileUserDAO fileUserDAO = new FileUserDAO();
        UserDTO userDTO = new UserDTO("Jordi", "2202", "vidalcerda17@gmail.com"
                , LocalDate.parse("1990-12-12"));

        UserDTO userDTO1 = new UserDTO("acoloma", "$asasasjdksdj", "acoloma@cipfpbatoi.es"
                , LocalDate.of(2002, 2, 22));

        try {
            ArrayList<UserDTO> userDTOS = fileUserDAO.findAll();
            System.out.println(userDTOS);
        }catch (IOException exception){
            exception.getStackTrace();
        }

        try {
            System.out.println(fileUserDAO.findByEmail("rperez@alumnos.es"));
        }catch (IOException exception){
            exception.getStackTrace();
        }

        fileUserDAO.delete("vidalcerda17@gmail.com");
        fileUserDAO.saveUser(userDTO);
        fileUserDAO.saveUser(userDTO1);


    }
}
