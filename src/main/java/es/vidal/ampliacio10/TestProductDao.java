package es.vidal.ampliacio10;

import java.io.IOException;
import java.util.ArrayList;

public class TestProductDao {

    public static void main(String[] args) {

        CSVProductDAO csvProductDAO = new CSVProductDAO();

        try {
            System.out.println("""
                    Listar todos los productos
                    ==========================
                    """);
            ArrayList<ProductDTO> productDTOArrayList = csvProductDAO.findAll();
            System.out.println(productDTOArrayList);
            System.out.println();
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }

        System.out.println("""
                    Buscar por código
                    ==========================
                    """);
        ProductDTO productDTO1 = csvProductDAO.findByCod("AR-004");
        System.out.println(productDTO1);
        System.out.println();

        System.out.println("""
                    Buscar por nombre
                    ==========================
                    """);
        ArrayList<ProductDTO> productDTOArrayList1 = csvProductDAO.findByName("Kunquat");
        System.out.println(productDTOArrayList1);
        System.out.println();

        System.out.println("""
                    Buscar por categoría
                    ==========================
                    """);
        ArrayList<ProductDTO> productDTOArrayList2 = csvProductDAO.findByCategory("Frutales");
        System.out.println(productDTOArrayList2);

    }
}
