package es.vidal.ampliacio10;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;

public class CSVProductDAO {

    private final File FILE;

    public CSVProductDAO(){

        FILE = new File("resources/ampliacio10/productos.csv");

    }

    public ArrayList<ProductDTO> findAll() throws IOException {
        ArrayList<ProductDTO> productDTOArrayList = new ArrayList<>();
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                String line = registerToString(record);
                ProductDTO productDTO = createProductDTO(line);
                productDTOArrayList.add(productDTO);
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return productDTOArrayList;
    }

    public String registerToString(CSVRecord record){
        return record.get(0) +
                "," +
                record.get(1) +
                "," +
                record.get(2) +
                "," +
                record.get(3) +
                "," +
                record.get(4) +
                "," +
                record.get(5);
    }

    public ProductDTO createProductDTO(String line){
        String[] productDTO = line.split(",");
        String cod = productDTO[0];
        String name = productDTO[1];
        String category = productDTO[2];
        int stockQuantity = Integer.parseInt(productDTO[3].trim());
        double salePrice = Double.parseDouble(productDTO[4]);
        double supplierPrice = Double.parseDouble(productDTO[5]);
        return new ProductDTO( cod, name, category, stockQuantity, salePrice, supplierPrice);
    }

    public ProductDTO findByCod(String cod){
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                String line = registerToString(record);
                ProductDTO productDTO = createProductDTO(line);
                if (productDTO.getCod().equals(cod)){
                    return productDTO;
                }
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return null;
    }

    public ArrayList<ProductDTO> findByName(String name){
        ArrayList<ProductDTO> productDTOArrayList = new ArrayList<>();
        String line;
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                line = registerToString(record);
                ProductDTO productDTO = createProductDTO(line);
                if (productDTO.getName().contains(name)){
                    productDTOArrayList.add(productDTO);
                }
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return productDTOArrayList;
    }

    public ArrayList<ProductDTO> findByCategory(String category){
        ArrayList<ProductDTO> productDTOArrayList = new ArrayList<>();
        String line;
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                line = registerToString(record);
                ProductDTO productDTO = createProductDTO(line);
                if (productDTO.getCategory().equals(category)){
                    productDTOArrayList.add(productDTO);
                }
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return productDTOArrayList;
    }
}
