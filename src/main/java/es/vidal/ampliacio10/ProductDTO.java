package es.vidal.ampliacio10;

import java.util.Objects;

public class ProductDTO {

    private String cod;

    private String name;

    private String category;

    private int stockQuantity;

    private double salePrice;

    private double supplierPrice;

    public ProductDTO(String cod, String name, String category, int stockQuantity, double salePrice, double supplierPrice) {
        this.cod = cod;
        this.name = name;
        this.category = category;
        this.stockQuantity = stockQuantity;
        this.salePrice = salePrice;
        this.supplierPrice = supplierPrice;
    }

    public String getCod() {
        return cod;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "cod = " + cod + '\'' +
                ", name = " + name + '\'' +
                ", category = " + category + '\'' +
                ", stockQuantity = " + stockQuantity +
                ", salePrice = " + salePrice +
                ", supplierPrice = " + supplierPrice +
                "}\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDTO that = (ProductDTO) o;
        return cod.equals(that.cod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cod);
    }
}
